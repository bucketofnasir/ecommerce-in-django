from django.contrib import admin
from .models import Category, Product, SubCategory, Review, Cart, Order

# Register your models here.
admin.site.register(Category)

admin.site.register(SubCategory)


class ProductModel(admin.ModelAdmin):
    list_display = ['__str__', 'sub_category', 'regular_price', 'sale_price', 'quantity']
    list_filter = ['sub_category', 'date']
    list_per_page = 10
    search_fields = ['__str__']

    class Meta:
        model = Product


admin.site.register(Product, ProductModel)


class ReviewModel(admin.ModelAdmin):
    list_display = ['__str__', '__product__', 'rating']
    list_filter = ['rating', 'date']
    list_per_page = 10
    search_fields = ['__str__','__product__']

    class Meta:
        model = Review


admin.site.register(Review, ReviewModel)


class CartModel(admin.ModelAdmin):
    list_display = ['__str__', 'products', 'quantity']
    list_filter = ['quantity', 'timestamp']
    list_per_page = 10
    search_fields = ['__str__','products']

    class Meta:
        model = Cart


admin.site.register(Cart, CartModel)


class OrderModel(admin.ModelAdmin):
    list_display = ['__str__', 'products', 'quantity',"total"]
    list_filter = ['status','quantity', 'timestamp']
    list_per_page = 10
    search_fields = ['__str__','products']

    class Meta:
        model = Order


admin.site.register(Order, OrderModel)
