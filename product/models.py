from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.auth.models import User
from account.models import Customer


# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=50)
    image = models.FileField()
    description = RichTextUploadingField(blank=True, null=True)

    def __str__(self):
        return self.name


class SubCategory(models.Model):
    name = models.CharField(max_length=50)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    description = models.TextField()

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=100)
    sub_category = models.ForeignKey(SubCategory, on_delete=models.CASCADE, related_name="product_category")
    featured_image = models.FileField()
    second_image = models.ImageField()
    third_image = models.ImageField(null=True, blank=True)
    description = RichTextUploadingField()
    regular_price = models.DecimalField(max_digits=20, decimal_places=2)
    sale_price = models.DecimalField(max_digits=20, decimal_places=2)
    sku = models.CharField(max_length=20)
    quantity = models.IntegerField()
    product_information = RichTextUploadingField()
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.name


class Review(models.Model):
    name = models.ForeignKey(Customer, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    rating = models.IntegerField(help_text="Give rating 1-5")
    comment = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name.get_full_name()

    def __product__(self):
        return self.product.name


class Cart(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    products = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.customer.name.get_full_name()


class Order(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    products = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.DecimalField(max_digits=10, decimal_places=2)
    subtotal = models.DecimalField(max_digits=10, decimal_places=2)
    total = models.DecimalField(max_digits=10, decimal_places=2)
    timestamp = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=50, choices=(("processing", "processing"), ("completed", "completed")),
                              default="processing")

    def __str__(self):
        return self.customer.name.get_full_name()
