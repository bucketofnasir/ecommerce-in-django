# Generated by Django 2.0.4 on 2018-05-27 08:30

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0016_auto_20180526_1645'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='second_image',
            field=models.ImageField(default=django.utils.timezone.now, upload_to=''),
            preserve_default=False,
        ),
    ]
