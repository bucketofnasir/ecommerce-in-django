# Generated by Django 2.0.4 on 2018-05-25 17:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0012_review'),
    ]

    operations = [
        migrations.AlterField(
            model_name='review',
            name='rating',
            field=models.IntegerField(help_text='Give rating 1-5'),
        ),
    ]
