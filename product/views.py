from django.shortcuts import render, get_object_or_404, Http404, redirect
from django.contrib.auth.models import User
from django.db.models import Q
from .models import Category, Product, SubCategory, Review, Cart, Order
from account.models import Customer, Shipping
from customizeSite.models import HomePageCarousel
from django.views import View
import datetime


# Create your views here.
class index(View):
    def get(self, request):
        cat = Category.objects.all()
        subcategory = SubCategory.objects.all()
        product = Product.objects.all()
        carousel = HomePageCarousel.objects.all()
        usr = User.objects.all().count()
        context = {
            "category": cat,
            "products": product,
            "subcategory": subcategory,
            "carousel": carousel,
            "usr": usr
        }
        return render(request, "index.html", context)


class GetShop(View):
    def get(self, request):
        c = request.GET.get('cat')
        s = request.GET.get('sub')
        search = request.GET.get('s')
        pr = Product.objects.all()
        cat = Category.objects.all()
        sub = SubCategory.objects.all()
        if c:
            pr = pr.filter(Q(sub_category__category__name__iexact=c))
        if s:
            pr = pr.filter(Q(sub_category__name__iexact=s))
        if search:
            pr = pr.filter(
                Q(name__icontains=search)
            )
        if request.user.is_authenticated:
            cart = Cart.objects.filter(customer__id=request.user.id)
        else:
            cart = None
        context = {
            "product": pr,
            "category": cat,
            "subcategory": sub,
            "cart": cart
        }
        return render(request, "shop.html", context)

    def post(self, request):
        pr = request.POST.get('id')
        try:
            product = get_object_or_404(Product, id=pr)
        except:
            raise Http404('Product not found')
        try:
            usr = get_object_or_404(User, id=request.user.id)
        except:
            raise Http404('User not found')
        if 'cart' in request.POST:
            q = 1
            instance = Cart.objects.create(customer=usr, products=product, quantity=q)
            instance.save()
            return redirect('product:shop')
        else:
            return redirect('product:shop')


class getSingleProduct(View):
    def get(self, request, id):
        try:
            product = get_object_or_404(Product, id=id)
            added_on = datetime.date(product.date.year, product.date.month, product.date.day)
            old = datetime.date.today() - added_on
            old = old.days
            cat = Category.objects.all()
            sub = SubCategory.objects.all()
            similar = Product.objects.filter(sub_category__name=product.sub_category.name).exclude(id=product.id)[:4]
            review = Review.objects.filter(product__id=id)
            try:
                cart = Cart.objects.filter(customer__name__id=request.user.id, products__id=id)
            except:
                cart = None
            context = {
                "category": cat,
                "subcategory": sub,
                "product": product,
                "old": old,
                "similar": similar,
                "review": review,
                "cart": cart
            }
            return render(request, "single.html", context)
        except:
            raise Http404('Product is not found')

    def post(self, request, id):
        try:
            product = get_object_or_404(Product, id=id)
        except:
            raise Http404('Product not found')
        try:
            usr = get_object_or_404(Customer, name__id=request.user.id)
        except:
            raise Http404('User not found')
        if "review" in request.POST:
            rating = request.POST.get('rating')
            comment = request.POST.get('comment')
            obj = Review.objects.create(name=usr, product=product, rating=rating, comment=comment)
            if obj:
                obj.save()
                return redirect('product:single', id=id)
            else:
                return redirect('product:single', id=id)

        if 'cart' in request.POST:
            q = request.POST.get('quantity')
            instance = Cart.objects.create(customer=usr, products=product, quantity=q)
            instance.save()
            return redirect('product:single', id=id)
        else:
            return redirect('product:single', id=id)


class getCart(View):
    def get(self, request):
        if request.user.is_authenticated is None:
            return redirect('account:login')
        delete = request.GET.get('action')
        if delete:
            id = request.GET.get('id')
            instance = get_object_or_404(Cart, id=id)
            instance.delete()
            return redirect('product:cart')
        query = Cart.objects.filter(customer__name__id=request.user.id)
        total = 0
        if query:
            for q in query:
                total = total + (q.products.sale_price * q.quantity)
        context = {
            "query": query,
            "total": total,
        }
        return render(request, "cart.html", context)


class Checkout(View):
    def get(self, request):
        if request.user.is_authenticated:
            customer = get_object_or_404(Customer, name__id=request.user.id)
            cart = Cart.objects.filter(customer__name__id=customer.name.id)
            shipping = get_object_or_404(Shipping, customer__id=customer.id)
            context = {
                "customer": customer,
                "cart": cart,
                "shipping": shipping
            }
            return render(request, 'checkout.html', context)
        else:
            return redirect('account:login')

    def post(self, request):
        usr = get_object_or_404(Customer, name__username=request.user.username)
        cart = Cart.objects.filter(customer__id=usr.id)
        if cart:
            for c in cart:
                pr = get_object_or_404(Product, id=c.products.id)
                sale = pr.sale_price * c.quantity
                obj = Order.objects.create(customer=usr, products=pr, quantity=c.quantity, subtotal=sale, total=sale,
                                           status="processing")
                obj.save()
            cart.delete()
        else:
            Http404('Sorry! Your cart is empty')
        return redirect('product:shop')
