from django.contrib import admin
from .models import HomePageCarousel, ContactMessage


# Register your models here.
class HomeCarouselModel(admin.ModelAdmin):
    list_display = ["__str__", "short_description"]
    list_per_page = 10
    search_fields = ["__str__"]

    class Meta:
        model = HomePageCarousel


admin.site.register(HomePageCarousel, HomeCarouselModel)


class ContactMessageModel(admin.ModelAdmin):
    list_display = ["__str__", "subject"]
    list_per_page = 10
    search_fields = ["__str__", 'subject']

    class Meta:
        model = ContactMessage


admin.site.register(ContactMessage, ContactMessageModel)
