from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.models import User
from django.views import View
from .form import ContactForm


# Create your views here.
class GetContact(View):
    template = "contact.html"

    def get(self, request):
        form = ContactForm
        info = get_object_or_404(User, id=1)
        return render(request, self.template, {"info": info, "form": form})

    def post(self, request):
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('site:contact')
