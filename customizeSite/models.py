from django.db import models


# Create your models here.
class HomePageCarousel(models.Model):
    title = models.CharField(max_length=50, help_text="Character must not more than 50")
    short_description = models.CharField(max_length=100, help_text="Character must not more than 100")
    background_image = models.ImageField()
    button_text = models.CharField(max_length=20)
    button_url = models.URLField(help_text="Enter a valid url")

    def __str__(self):
        return self.title


class ContactMessage(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField()
    subject = models.CharField(max_length=200)
    message = models.TextField()

    def __str__(self):
        return self.name
