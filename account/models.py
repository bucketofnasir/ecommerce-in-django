from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse


# Create your models here.
class Customer(models.Model):
    name = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.FileField(help_text="Please upload a profile picture")
    birth_date = models.DateField()
    phone = models.CharField(max_length=100, default="")

    def __str__(self):
        return self.name.username

    def get_absolute_url(self):
        return reverse('user', kwargs={'username': self.name.username})


class Shipping(models.Model):
    customer = models.OneToOneField(Customer, on_delete=models.CASCADE)
    city = models.CharField(max_length=50)
    area = models.CharField(max_length=50)
    address = models.TextField()
    alternative_mobile_no = models.CharField(max_length=20, null=True, blank=True)

    def __str__(self):
        return self.customer.name.get_full_name()
