"""ecommerce URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from . import views
app_name='account'
urlpatterns = [
    path('login', views.getLogin.as_view(), name="login"),
    path('register', views.getRegister.as_view(), name="register"),
    path('changepass', views.PasswordChange.as_view(), name="change_password"),
    path('logout', views.getLogout.as_view(), name="logout"),
    path('activate/<uidb>/<token>',views.activate, name='activate'),
    path('profile',views.getProfile.as_view(), name='profile'),
    path('profile/create', views.CustomerAccount.as_view(), name='account'),
    path('shipping/create', views.CustomerShipping.as_view(), name='create_shipping'),
    path('shipping/update', views.UpdateShipping.as_view(), name='update_shipping'),

]
