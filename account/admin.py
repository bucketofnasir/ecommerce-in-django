from django.contrib import admin
from .models import Customer, Shipping


# Register your models here.
class CustomerModel(admin.ModelAdmin):
    list_display = ["__str__", "birth_date", "phone"]
    list_filter = ["birth_date"]
    fieldsets = [(None, {'fields': ['name', 'image']}),
                 ("Patient info", {'fields': ['birth_date']}),
                 ("Patient Contact info", {'fields': ['phone']})
                 ]
    list_per_page = 10
    search_fields = ["__str__", "phone"]

    class Meta:
        model = Customer


admin.site.register(Customer, CustomerModel)


class ShippingModel(admin.ModelAdmin):
    list_display = ["__str__"]
    list_per_page = 10
    search_fields = ["__str__"]

    class Meta:
        model = Shipping


admin.site.register(Shipping, ShippingModel)
