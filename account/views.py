from django.shortcuts import render, get_object_or_404, Http404, redirect, HttpResponse
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.views import View
from .form import SignUpForm, CustomerAccountCreateForm, ShippingCreateForm
from .models import Customer
from .models import Shipping
from product.models import Cart

from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import send_mail
from django.conf import settings


# Create your views here.


class getLogin(View):
    def get(self, request):
        if request.user.is_authenticated:
            return redirect('product:index')
        return render(request, 'login.html')

    def post(self, request):
        username = request.POST.get('username')
        password = request.POST.get('password')
        auth = authenticate(request, username=username, password=password)
        if auth is not None:
            login(request, auth)
            return redirect('product:index')
        else:
            return redirect('account:login')


class getRegister(View):
    def get(self, request):
        if request.user.is_authenticated:
            return redirect('product:index')
        form = SignUpForm
        return render(request, 'register.html', {"form": form})

    def post(self, request):
        form = SignUpForm(request.POST or None)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.is_active = False
            instance.save()
            current_site = get_current_site(request)
            mail_subject = 'VMA: Activate your account.'
            message = render_to_string('mail_confirm_message.html', {
                'user': instance,
                'domain': current_site.domain,
                'uid': instance.pk,
                'token': account_activation_token.make_token(instance)
            })
            to_email = form.cleaned_data.get('email')
            to_list = [to_email]
            from_email = settings.EMAIL_HOST_USER

            send_mail(mail_subject, message, from_email, to_list, fail_silently=True)
            return HttpResponse('<h3>A confirmation mail was sent. Please check inbox and confirm your account to '
                                'login</h3>')
        else:
            return redirect('account:register')


class PasswordChange(View):
    def get(self, request):
        if request.user.is_authenticated:
            form = PasswordChangeForm(request.user, request.POST or None)
            context = {
                "form": form,
                "btn": "Save Changes",
                "title": "Change your password"
            }
            return render(request, 'form.html', context)
        else:
            return redirect('account:login')

    def post(self, request):
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            return redirect('account:profile')
        else:
            return redirect('account:change_password')


def activate(request, uidb, token):
    try:
        user = User.objects.get(pk=uidb)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        return HttpResponse(
            'Thank you for your email confirmation. Now you can <a href="/login" %}">login</a> your account.')
    else:
        return HttpResponse('Activation link is invalid!')


class getLogout(View):
    def get(self, request):
        logout(request)
        return redirect('product:index')


class getProfile(View):
    def get(self, request):
        if request.user.is_authenticated:
            try:
                usr = get_object_or_404(Customer, name__id=request.user.id)
            except:
                return redirect('account:account')
            query = Cart.objects.filter(customer__id=usr.id)
            try:
                shipping = get_object_or_404(Shipping, customer__name__id=request.user.id)
            except:
                return redirect('account:create_shipping')
            total = 0
            if query:
                for q in query:
                    total = total + (q.products.sale_price * q.quantity)
            context = {
                "user": usr,
                "query": query,
                "total": total,
                "shipping": shipping,
            }
            return render(request, 'profile.html', context)
        else:
            return redirect('account:login')


class CustomerAccount(View):
    def get(self, request):
        form = CustomerAccountCreateForm
        context = {
            "btn": "Create Account",
            "form": form,
            "title": "Create customer account"
        }
        return render(request, 'form.html', context)

    def post(self, request):
        form = CustomerAccountCreateForm(request.POST, request.FILES)
        if form.is_valid():
            cst = get_object_or_404(User, id=request.user.id)
            instance = form.save(commit=False)
            instance.name = cst
            instance.save()
            return redirect('account:profile')
        else:
            return redirect('account:create_shipping')


class CustomerShipping(View):
    def get(self, request):
        if request.user.is_authenticated is None:
            return redirect('account:login')
        form = ShippingCreateForm
        context = {
            "btn": "Create shipping",
            "form": form,
            "title": "Create shipping location"
        }
        return render(request, 'form.html', context)

    def post(self, request):
        form = ShippingCreateForm(request.POST)
        if form.is_valid():
            cst = get_object_or_404(Customer, name__id=request.user.id)
            instance = form.save(commit=False)
            instance.customer = cst
            instance.save()
            return redirect('account:profile')
        else:
            return redirect('account:create_shipping')


class UpdateShipping(View):
    def get(self, request):
        if request.user.is_authenticated is None:
            return redirect('account:login')
        usr = get_object_or_404(Shipping, customer__name__id=request.user.id)
        form = ShippingCreateForm(request.POST or None, instance=usr)
        context = {
            "btn": "Update shipping",
            "form": form,
            "title": "Update shipping location"
        }
        return render(request, 'form.html', context)

    def post(self, request):
        form = ShippingCreateForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('account:profile')
        else:
            return redirect('account:update_shipping')
